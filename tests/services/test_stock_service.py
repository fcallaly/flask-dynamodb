import boto3
import pytest

from server.services import stock_service


class DummyAppContext:
    pass


@pytest.fixture
def dummy_app():
    dummy_app = DummyAppContext()
    stock_service.current_app = dummy_app
    return dummy_app


def test_init_ddb(dummy_app, mocker):
    mocker.patch('boto3.resource')

    stock_service.init_ddb()

    boto3.resource.assert_called_once_with('dynamodb')


def test_get_all(dummy_app, mocker):
    items = ['AAPL', 'AMZN']
    dummy_app._ddb_table = mocker.MagicMock()
    dummy_app._ddb_table.scan.return_value = {'Items': items}

    assert stock_service.get_all() == items


def test_get_stock(dummy_app, mocker):
    item = 'AAPL'
    dummy_app._ddb_table = mocker.MagicMock()
    dummy_app._ddb_table.get_item.return_value = {'Item': item}

    assert stock_service.get_stock('AMZN') == item


def test_create_stock(dummy_app, mocker):
    put_response = {'ResponseMetadata': {'HTTPStatusCode': 200}}

    mocker.patch('server.services.stock_service.get_stock',
                 return_value=None)

    dummy_app._ddb_table = mocker.MagicMock()
    dummy_app._ddb_table.put_item.return_value = put_response

    stock_service.create_stock({'ticker': 'AAPL'})


def test_create_stock_already_exists(dummy_app, mocker):
    mocker.patch('server.services.stock_service.get_stock',
                 return_value='AAPL')

    with pytest.raises(Exception):
        stock_service.create_stock({'ticker': 'AAPL'})
