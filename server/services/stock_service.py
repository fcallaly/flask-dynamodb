import logging

import boto3
from flask import current_app

LOG = logging.getLogger(__name__)


def init_ddb():

    if hasattr(current_app, '_ddb') and hasattr(current_app, '_ddb_table'):
        return

    LOG.info('Initialising DynamoDB resource')
    current_app._ddb = boto3.resource('dynamodb')
    current_app._ddb_table = current_app._ddb.Table('Stocks')


def get_all(limit=10, esk=None):
    LOG.debug('get all')

    if esk:
        response = current_app._ddb_table.scan(Limit=limit,
                                               ExclusiveStartKey=esk)
    else:
        response = current_app._ddb_table.scan(Limit=limit)

    LOG.debug('Received ' + str(response) + ' from DynamoDB')

    if 'Items' not in response:
        return None

    return response['Items']


def get_stock(ticker):
    LOG.debug('get: ' + str(ticker))

    response = current_app._ddb_table.get_item(Key={'ticker': ticker})

    LOG.debug('Received ' + str(response) + ' from DynamoDB')

    if 'Item' not in response:
        return None

    return response['Item']


def create_stock(stock_resource):
    LOG.debug('create: ' + str(stock_resource))

    if get_stock(stock_resource['ticker']):
        LOG.debug('Stock already exists')
        raise Exception('Stock already exists ' +
                        str(stock_resource['ticker']))

    response = current_app._ddb_table.put_item(Item=stock_resource)

    LOG.debug('Received ' + str(response) + ' from DynamoDB')

    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        LOG.debug('Error creating new Stock')
        raise Exception('Error creating new Stock: ' + str(stock_resource))

    return
